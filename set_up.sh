#!/bin/env bash
cd ..
git clone https://github.com/SiLab-Bonn/basil
git clone https://github.com/SiLab-Bonn/online_monitor.git
git clone ssh://git@gitlab.cern.ch:7999/mightypix/telepix_daq.git
cd telepix_daq && git checkout development_v2 && cd ..
cd telepix_daq/telepix_daq/ && git clone git@github.com:BeeBeansTechnologies/SiTCP_Netlist_for_Kintex7.git
